/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alancel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/02 19:07:57 by alancel           #+#    #+#             */
/*   Updated: 2020/02/02 19:26:27 by alancel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	line(int length, char a, char b)
{
	int i;

	i = 0;
	ft_putchar(a);
	while (i < length - 2)
	{
		ft_putchar(b);
		i++;
	}
	if (length > 1)
	{
		ft_putchar(a);
	}
	ft_putchar('\n');
}

void	rush(int x, int y)
{
	int i;

	i = 0;
	if (x <= 0 || y <= 0)
	{
		return ;
	}
	line(x, 'o', '-');
	while (i < y - 2)
	{
		line(x, '|', ' ');
		i++;
	}
	if (y > 1)
	{
		line(x, 'o', '-');
	}
}
