/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 18:18:56 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/06 22:21:02 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int len;
	int i;

	len = max - min;
	if (len <= 0)
		return (0);
	*range = (int *)malloc(sizeof(int) * (len + 1));
	i = -1;
	while (++i < len)
		range[0][i] = min + i;
	return (0);
}
