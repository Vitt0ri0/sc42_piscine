#include "ft_ultimate_range.c"
#include <stdio.h>

void test_range(int min, int max)
{
	int *array;

	ft_ultimate_range(&array, min, max);

	for (int i = 0; i < max-min; i++)
		printf("%d ", array[i]);
	printf("\n");
}

int main()
{
	test_range(0, 0);
	test_range(0, -1);
	test_range(1, 2);
	test_range(0, 11);
	test_range(-100, -80);
	return 0;
}
