/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 17:36:55 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/06 22:17:21 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int *array;
	int len;
	int i;

	len = max - min;
	if (len <= 0)
		return (NULL);
	array = malloc(sizeof(int) * (len + 1));
	i = -1;
	while (++i < len)
	{
		array[i] = min + i;
	}
	return (array);
}
