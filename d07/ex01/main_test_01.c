#include "ft_range.c"
#include <stdio.h>

void test_range(int min, int max)
{
	int *array;

	array = ft_range(min, max);

	int i = -1;
	while (array[++i])
		printf("%d", array[i]);
	//for (int i = 0; i < max-min; i++)
	//	printf("%d ", array[i]);
	printf("\n");
}

int main()
{
	test_range(0, 0);
	test_range(0, -1);
	test_range(0, 1);
	test_range(0, 11);
	test_range(-100, -80);
	return 0;
}
