#include "ft_putstr.c"

int main() {

	ft_putstr("hello world");
	ft_putstr("hello\tme\nworld");
	ft_putstr("hello\0world\n");
	char s[10];
	s[0] = 'h';
	s[1] = 'e';
	s[2] = '\0';
	s[3] = 'z';
	ft_putstr(s);
}
