/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 17:32:24 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/04 19:55:19 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print(char *s)
{
	while (*s)
	{
		ft_putchar(*s);
		s++;
	}
}

char	get_rank(int *n)
{
	int val = *n % 10;
	*n = *n / 10;
	char c;
	c = val + 48;
	return c;
}

void	reverse(char	*str)
{
	int i;
	char *new;

	i = 0;
	while (*str)
	{
		i++;
		str++;
	}

	while (i >= 0)
	{
		*new = *str;
		i--;
		str--;
		new++;
	}
	str = new;
}

void	ft_putnbr(int nb)
{
	char *s;
	char *start = s;

	if (nb == 0)
	{
		s = "0";
		print(s);
	}
	if (nb > 0)
	{
		*s = get_rank(&nb);
		s++;		
	}
	*s = '\0';
	print(start);
}


