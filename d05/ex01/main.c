#include "ft_putnbr.c"
#include <stdio.h>

void test(int num)
{
	printf("%d:", num);
	ft_putnbr(num);
	printf("\n");
}

int main() 
{
	test(-321);
	test(123);
	test(-11);
	test(-10);
	test(-9);
	test(-2);
	test(-1);
	test(0);
	test(1);
	test(2);
	test(12);
	test(0x7FFFFFFE);
	test(0x7FFFFFFF);
	test(0x80000000);
	test(0x80000001);
	test(0xFFFFFFFF);
	test(0xFFFFFFFE);	
}
