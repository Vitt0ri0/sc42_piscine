/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 17:52:33 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/16 01:18:12 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

#define TERM '\0'

#ifndef FT_LIB_H
# define FT_LIB_H

int		ft_atoi(char *str);

void	ft_putchar(char c);

void	ft_putnbr(int nb);

void	ft_putstr(char *str);

# ifndef FT_LIST_H
#  define FT_LIST_H

typedef struct		s_list
{
	struct s_list	*next;
	void			*data;
}					t_list;

# endif

t_list	*ft_list_create_elem(void *data);
void	ft_list_print(t_list *list);
void	ft_list_push_back(t_list *begin, void *data);

#endif

char	**create_array_empty(int len_max, int wid_max);
char	**create_array_of_chars(int len_max, int wid_max, char c);
void	colle_print_lines(char **lines);
void	colle_print_line(char *line);
void	ft_putchar_malloc(char **lines, int *len, int *wid, char c);
char	*ft_malloc_a_by_b(int x, int y);
char	*ft_realloc(char *old_line, unsigned int *buf_size);
