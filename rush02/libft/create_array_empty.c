/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_empty_array.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 23:57:58 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/16 01:17:49 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**create_array_empty(int len_max, int wid_max)
{
	int		l;
	int		w;
	char	**lines;

	len_max++;
	wid_max++;
	l = 0;
	w = 0;
	lines = (char **)malloc(len_max * sizeof(char *));
	while (l < len_max)
	{
		lines[l] = (char *)malloc(wid_max * sizeof(char));
		l++;
	}
	return (lines);
}

char	**create_array_of_chars(int len_max, int wid_max, char c)
{
	int		l;
	int		w;
	int		ww;
	char	**lines;

	len_max++;
	wid_max++;
	l = 0;
	w = 0;
	ww = 0;
	lines = (char **)malloc(len_max * sizeof(char *));
	while (l < len_max)
	{
		lines[l] = (char *)malloc(wid_max * sizeof(char));
		ww = -1;
		while (++ww < wid_max)
		{
			lines[l][ww] = c;
			if (ww + 1 == wid_max)
				lines[l][ww] = '\0';
		}
		l++;
	}
	lines[--l][0] = '\0';
	return (lines);
}
