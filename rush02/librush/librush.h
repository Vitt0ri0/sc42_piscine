/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header_rush.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 18:46:29 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/16 01:27:11 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	rush04(int x, int y);
void	rush03(int x, int y);
void	rush02(int x, int y);
void	rush01(int x, int y);
void	rush00(int x, int y);

void	line0(int length, char a, char b);

char	*rush00m(int x, int y);
char 	*rush01m(int x, int y);
char 	*rush02m(int x, int y);
char 	*rush03m(int x, int y);
char 	*rush04m(int x, int y);

void	ft_putchar_m(char c, char *line, int *pos);
void	print_line_0(int length, char a, char b, char *line, int *pos);
char 	*rush_caller(int rush_number, int len, int wid);
