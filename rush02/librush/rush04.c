/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 19:00:47 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/16 19:00:51 by tfarenga         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "librush.h"

void	print_char04(int countx, int county, int x, int y)
{
	if (countx == 1 && county == 1)
		ft_putchar('A');
	else if (y != 1 && x != 1 && countx == x && county == y)
		ft_putchar('A');
	else if (countx == 1 && county == y)
		ft_putchar('C');
	else if (countx == x && county == 1)
		ft_putchar('C');
	else if ((countx == 1 || countx == x) && county != 1 && county != y)
		ft_putchar('B');
	else if ((county == 1 || county == y) && countx != 1 && countx != x)
		ft_putchar('B');
	else if (county > 1 && county < y && countx > 1 && countx < x)
		ft_putchar(' ');
}

void	rush04(int x, int y)
{
	int county;
	int countx;

	county = 0;
	while (++county <= y)
	{
		countx = 0;
		while (++countx <= x)
		{
			print_char04(countx, county, x, y);
		}
		ft_putchar('\n');
	}
}
