/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 19:00:12 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/17 20:48:33 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "librush.h"
void	print_char_1(int countx, int county, int x, int y, char *line, int *pos)
{
	if (countx == 1 && county == 1)
		ft_putchar_m('/', line, pos);
	else if (y != 1 && x != 1 && countx == x && county == y)
		ft_putchar_m('/', line, pos);
	else if ((countx == 1 && county == y) || (countx == x && county == 1))
		ft_putchar_m('\\', line, pos);
	else if ((countx == 1 || countx == x) && county != 1 && county != y)
		ft_putchar_m('*', line, pos);
	else if ((county == 1 || county == y) && countx != 1 && countx != x)
		ft_putchar_m('*', line, pos);
	else if (county > 1 && county < y && countx > 1 && countx < x)
		ft_putchar_m(' ', line, pos);
}

char	*rush01m(int x, int y)
{
	char *line;
	int county;
	int countx;
	int p;
	int *pos;

	p = 0;
	pos = &p;
	line = ft_malloc_a_by_b(x, y);
	county = 0;
	while (++county <= y)
	{
		countx = 0;
		while (++countx <= x)
		{
			print_char_1(countx, county, x, y, line, pos);
		}
		ft_putchar_m('\n', line, pos);
	}
	return (line);
}
