/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header_colle.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 21:40:13 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/17 21:12:33 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "librush.h"

char	*parse_input_line();
void	print_colle_result(int rushes[], int len, int wid);
int		compare_lines(char *line_input, char *line_rush);
