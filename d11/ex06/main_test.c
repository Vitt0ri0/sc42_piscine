/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 17:47:13 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/12 20:58:40 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "headers.h"
#include "ft_list_last.c"

int	main()
{
	t_list *list;
	char *str = "hello";

	list = ft_create_elem(str);
	ft_list_push_front(&list, "world"); 
	ft_list_push_front(&list, "fuck your ass");

	ft_print_list(list);

	t_list *last;

	ft_putstr("last list element:\n");
	last = ft_list_last(list);
	ft_print_list(last);	
	return 0;
}	
