/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 21:07:56 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/12 23:13:52 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_clear(t_list **begin_list)
{
	t_list *list;
	t_list *tmp;

	if (!begin_list)
		return ;
	list = *begin_list;
	while (list && list->next)
	{
		tmp = list->next;
		free(list);
		list = tmp;
	}
	*begin_list = (void *)0;
}
