/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 21:11:13 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/12 23:06:02 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	t_list			*list;
	unsigned int	pos;

	list = begin_list;
	pos = 0;
	while (list && list->next && pos < nbr)
	{
		list = list->next;
		pos++;
	}
	if (pos == nbr)
		return (list);
	else
		return ((void*)0);
}
