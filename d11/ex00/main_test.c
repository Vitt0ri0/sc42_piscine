/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 17:47:13 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/12 19:28:23 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_create_elem.c"
#include "headers.h"

int	main()
{
	t_list *list;
	char *str = "hello";

	list = ft_create_elem(str);

	ft_print_list(list);

	return 0;
}	
