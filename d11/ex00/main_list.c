#include <stdlib.h>
#include "list.h"
#include "headers.h"
int main()
{
	t_list *list;

	list = NULL;

	list = add_link(list, "elem1\n");
	//list = add_link(list, "elem2\n");
	//list = add_link(list, "elem3\n");

	print_list(list);

	return 0;
}

t_list *add_link(t_list *list, char *str)
{
	t_list *new;
	t_list *last;

	if (!list)
	{
		list = malloc(sizeof(t_list));
		if (new)
		{
			new->str = str;
			new->next = NULL;
		}
		return list;
	}

	last = list;
	while (last->next != NULL)
		last = last->next;

	new = malloc(sizeof(t_list));
	if (new)
	{
		new->str = str;
		new->next = NULL;
		last->next = new;
		//list->next = new;
		//new->next = NULL;
	}
	return list;
}

void print_list(t_list *list)
{
	while (list)
	{
		ft_putstr(list->str);
		list = list->next;
	}
}
