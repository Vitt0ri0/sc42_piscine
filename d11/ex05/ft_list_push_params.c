/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 21:47:59 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/12 23:34:51 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

t_list	*ft_list_push_params(int ac, char **av)
{
	t_list	*prev;
	t_list	*list;
	int		counter;

	if (ac < 0)
		return (NULL);
	prev = NULL;
	counter = 0;
	while (counter < ac)
	{
		list = ft_create_elem(av[counter]);
		list->next = prev;
		prev = list;
		counter++;
	}
	return (list);
}
