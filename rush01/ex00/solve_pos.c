/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_pos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 15:57:12 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/09 15:57:16 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

extern int SIZE;
extern int DIM;

int *get_empty_mask()
{
    int i;
    int *mask;

    mask = (int*)malloc(sizeof(int) * (SIZE + 1));
    i = -1;
    while (++i < SIZE)
        mask[i] = 0;
    return mask;
}

int *solve_square(int **nums, int a, int b, int *mask)
{
    int i;
    int j;
    int num;
    int sa;
    int sb;

    sa = a / DIM;
    sb = b / DIM;
    i = -1;
    j = -1;
    while (++i < DIM)
        while (++j < DIM)
        {
            num = nums[i + sa * DIM][j + sb * DIM];
            mask[num - 1] += 1;
        }
    return mask;
}

int *solve_lines(int **nums, int a, int b, int *mask)
{
    int i;
    int num;

    /* vertical line */
    i = -1;
    while (++i < SIZE)
    {
        num = nums[i][b];
        mask[num - 1] += 1;
    }
    /* horizontal line */
    i = -1;
    while (++i < SIZE)
    {
        num = nums[a][i];
        mask[num - 1] += 1;
    }
    return mask;
}

int *convert_to_ints(int *mask)
{
    int *numbers;
    int i = -1;
    int j = 0;
    int len;

    len = 0;
    while (++i < SIZE)
    {
        if (mask[i] == 0)
            len++;
    }

//    printf("len: %d\n", len);
    /* we need (len + 1) 'cause int -1 as a terminator */
    numbers = (int*)malloc(sizeof(int) * (len + 1));

    i = -1;
    j = 0;
    while (++i < SIZE)
    {
        if (mask[i] == 0)
        {
            numbers[j] = i + 1;
            j++;
        }
    }
    numbers[j] = -1;
    return numbers;

}

int *solve_pos(int **nums, int a, int b)
{
    int *mask;
    int *numbers;

    mask = get_empty_mask();
    mask = solve_lines(nums, a, b, mask);
    mask = solve_square(nums, a, b, mask);

    numbers = convert_to_ints(mask);

    return numbers;
}
