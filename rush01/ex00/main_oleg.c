#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int SIZE = 9;
int DIM = 3;

int		testin(int argc, char **argv);
int		**parse_input(int argc, char **argv);
int	    sudoku_solver(int **nums, int *a, int *b);

void	print_nums(int *nums)
{
    printf("cand: ");
    int i = -1;
    while (nums[++i] != -1)
        printf("%d ", nums[i]);
    printf("\n");
}


void print_field(int **mask)
{
    int i=0;
    int j=0;
    char c;

    while (i < SIZE)
    {
        j = 0;
        while (j < SIZE)
        {
            printf("%d ", mask[i][j]);
            j++;
        }
        printf("\n");
        i++;
    }
    printf("\n");
}

int		main(int argc, char **argv)
{
	int result;
	int **nums;
	int *a;
	int *b;
	char c;


	*a = 0;
	*b = 0;
	result = 0;

	result = testin(argc, argv);
	if (result == 0)
    {
        nums = parse_input(argc, argv);
        result = sudoku_solver(nums, a, b);
    }

	if (result != 0)
        write(1, "Error\n", 6);
	return (0);
}
