/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_solver.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 22:26:11 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/09 13:54:59 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#define BASE 3

#include <stdio.h>
#include <stdlib.h>

int *solve_pos(int **nums, int a, int b);
void	print_nums(int *nums);
void print_field(int **mask);

//int SIZE = BASE * BASE;
extern int SIZE;

int *join_candidates(int *lines, int*square)
{
	int i;
	int j;
	int len;
	int *cand_mask;
	int *cand;

	cand_mask = (int *)malloc(sizeof(int) * SIZE);
	i = -1;
	len = 0;
	while (++i < SIZE)
		cand_mask[i] = 0;
	while (*lines != 0)
    {
        cand_mask[*lines] = 1;
        lines++;
    }
	while (*square != 0)
	{
		cand_mask[*square] += 1;
		if (cand_mask[*square] == 2)
			len++;
		square++;
	}
	printf("len= %d\n", len);
	i = -1;
	while (++i < SIZE)
        printf("%d ", cand_mask[i]);

	cand = (int *)malloc(sizeof(int) * (len + 1));
	i = 0;
	j = 0;
	while (i < SIZE)
	{
		if (cand_mask[i] == 2)
		{
			cand[j] = i;
			j++;
		}
		i++;
	}
	cand[j] = 0;
	return cand;
}

void get_next_pos(int *a, int *b)
{
    if (*b < SIZE - 1)
        *b += 1;
    else if (*b == SIZE - 1)    {
        if (*a < SIZE - 1)
        {
            *b = 0;
            *a += 1;
        }
    }
    else
        {
        *a = -1;
        *b = -1;
    }
}

void status(int **nums)
{
    char ch;
    print_field(nums);
    scanf("%c",&ch);
}

int	sudoku_solver(int **nums, int *a, int *b)
{
	int *numbers;

	if (*a == SIZE && *b == SIZE)
		return 0; /* correct solution */

    status(nums);
	numbers = solve_pos(nums, *a, *b);
    print_nums(numbers);
    while (*numbers != -1)
    {
        get_next_pos(a, b);
        printf("a, b: %d %d\n", *a, *b);
        sudoku_solver(nums, a, b);
        numbers++;
    }

    /* no solution */
    return -1;
}
