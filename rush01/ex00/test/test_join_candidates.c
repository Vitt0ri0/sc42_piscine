/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_tests.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 01:23:52 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/09 14:05:00 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int *join_candidates(int *lines, int *square);

void	test_join_candidates(int *lines, int *square)
{
	int *res;

	res = join_candidates(lines, square);

	while (*res != 0)
    {
        printf("%d ", *res);
        res++;
    }
}


int		*generate_line(int max_len)
{
	int *lines;
	int len = rand() % max_len;

	lines = (int*)malloc(sizeof(int) * (len + 1));
	int i = 0;
	while (i < len)
	{
		lines[i] = (rand() % max_len) + 1;
		i++;
	}
	lines[i] = 0;
	return lines;
}

void	print_nums(int *nums)
{
	int i = -1;
	while (nums[++i] != 0)
		printf("%d ", nums[i]);
}

int		main()
{
	srand(time(0));
	int *line1;
	int *line2;

	int len1 = 5;
	int len2 = 3;
	line1 = (int*)malloc(sizeof(int) * (len1 + 1));
	line2 = (int*)malloc(sizeof(int) * (len2 + 1));
	int i = -1;
	int j = -1;
	while (++i < len1)
	    line1[i] = i+1;
	line1[i] = 0;
	while (++j < len2)
	    line2[j] = j+1;
	line2[j] = 0;

	print_nums(line1);
	printf("\n");
	print_nums(line2);
	printf("\n");
	
	test_join_candidates(line1, line2);

}
	
