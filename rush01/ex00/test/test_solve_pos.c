/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_solve_pos.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 16:31:57 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/09 16:32:06 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "solve_pos.c"

//int DIM = 3;
//int SIZE = 9;

int **generate_field()
{
    int **a;

    a = (int**)malloc(sizeof(int*) * SIZE);
    int i = 0;
    while (i < SIZE)
    {
        int j = 0;
        a[i] = (int*)malloc(sizeof(int) * SIZE);
        while (j < SIZE)
        {
            a[i][j] = 0;
            j++;
        }
        i++;
    }
    a[0][1] = 1;
    a[0][5] = 4;
    a[0][7] = 9;
//
    a[3][0] = 2;
    a[6][0] = 3;
    a[7][0] = 4;

    return a;
}

void print_field(int **mask)
{
    int i=0;
    int j=0;
    while (i < SIZE)
    {
        j = 0;
        while (j < SIZE)
        {
            printf("%d ", mask[i][j]);
            j++;
        }
        printf("\n");
        i++;
    }
}

void print_mask(int *mask)
{
    int i = -1;
    while (++i < SIZE)
        printf("%d ", mask[i]);
    printf("\n");
}

void	print_nums(int *nums)
{
    int i = -1;
    while (nums[++i] != -1)
        printf("%d ", nums[i]);
}

int main()
{
    int **nums;
    int *mask;

    nums = generate_field();

    print_field(nums);

    mask = solve_pos(nums, 0, 0);

    printf("\n");
//    print_mask(mask);
    print_nums(mask);

    return 0;
}
