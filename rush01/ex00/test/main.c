/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmozell <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 17:33:03 by dmozell           #+#    #+#             */
/*   Updated: 2020/02/09 17:33:06 by dmozell          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//temp TODO remove
#include <stdio.h>

#include <unistd.h>
#include <stdlib.h>

int		**parse_input(char **argv);
int		sudoku_solver(int **nums);

void	print(char *str)
{
	while (*str)
		write(1, str, 1);
}

int		main(int argc, char **argv)
{
	int a;
	int **nums;
	int result;

	nums = parse_input(argv);

	printf("%d %d %d", nums[2][3], nums[4][5], nums[6][7]);

	result = sudoku_solver(nums);
	a = 0;
	//a = testin(argc, argv);
	//if (a < 2 || a > 10)
	//	write(1, "Error\n", 6); // todo: replace with ft_putstr()



	return (0);
}
