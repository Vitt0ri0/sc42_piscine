/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_input.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/08 22:37:54 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/08 23:37:59 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int **parse_input(char **lines)
{
	int **numbers;
	int len = 9;

	numbers = (int**) malloc(sizeof(int*) * len);

	for (int i = 0; i < 9; i++)
	{
		numbers[i] = (int*) malloc (sizeof(int) * len);

		for (int j = 0; j < 9; j++)
			numbers[i][j] = i*j;
	}
	return numbers;	
}
