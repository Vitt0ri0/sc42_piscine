#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

//void	print(int **a)
//{
//	int b;
//	int c;
//	b = 0;
//	while (b < 9)
//	{
//		c = 0;
//		while (c < 9)
//		{
//			write (1, 'a[b][c]', 1);
//			printf("%d", a[b][c]);
//			write (1, " ", 1);
//			c++;
//		}
//		b++;
//		write (1, "\n", 1);
//	}
//}

int		testin(int argc, char **argv)
{
	int i;
	
	i = 0;
	if (argc == 10)
	{
		while (argc > 1)
		{
			i = 0;
			while (argv[argc-1][i] != '\0')
			{
				if ((argv[argc-1][i] < 49 || argv[argc-1][i] > 57) && argv[argc-1][i] != 46)
					return (1);
				i++;
			}
			if (i != 9)
				return (1);
			argc--;
		}
		return (0);
	}
	return (1);
}

int		convert_char(char c)
{
	int i;
	
	if (c == 46)
		return (0);
	else
	{
		i = c - 48;
		return (i);
	}
}

int **parse_input(int argc, char **argv)
{
    char ch;
	int size = 9;
	int **a;
	a = (int**)malloc((size) *sizeof(int*));
	
	int i = 0;
	while (i < size)
	{
		a[i] = (int *)malloc((size) * sizeof(int));
		int j = 0;
		while (j < size)
		{
			a[i][j] = convert_char(argv[i+1][j]);
			j++;
		}
		i++;
	}
	return a;
}
