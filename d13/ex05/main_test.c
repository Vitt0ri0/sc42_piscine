#include "headers.h"
#include "btree_search_item.c"

int main(int argc, char **argv)
{
	t_btree *root = btree_create_simple_tree();
	t_btree *res;
	int (*fu)(void *, void *);
	char *s = argv[1];

	fu = &ft_strcmp_v;

	res = (t_btree*) btree_search_item(root, s, fu);
	if (res == (void *)0)
		ft_putstr("NULL");
	else
		ft_putstr(res);
}

