/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_search_item.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 22:10:28 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/14 23:41:09 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	*btree_search_item(t_btree *root, void *data_ref,
		int (*cmpf)(void *, void *))
{
	if (!root || !data_ref)
		return ((void *)0);
	if (root->left)
		return (btree_search_item(root->left, data_ref, cmpf));
	if (!cmpf(root->item, data_ref))
		return (root->item);
	if (root->right)
		return (btree_search_item(root->right, data_ref, cmpf));
	return ((void *)0);
}
