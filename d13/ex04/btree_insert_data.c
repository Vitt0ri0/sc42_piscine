/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 23:26:01 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/14 23:32:42 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_insert_data(t_btree **root, void *item,
		int (*cmpf)(void *, void *))
{
	t_btree *cur;

	cur = *root;
	if (*root && root)
	{
		if (cmpf(item, cur->item) >= 0)
			if (cur->right)
				btree_insert_data(&(cur->right), item, cmpf);
			else
				cur->right = btree_create_node(item);
		else if (cmpf(item, cur->item) < 0)
		{
			if (cur->left)
				btree_insert_data(&(cur->left), item, cmpf);
			else
				cur->left = btree_create_node(item);
		}
	}
	else
		cur = btree_create_node(item);
}
