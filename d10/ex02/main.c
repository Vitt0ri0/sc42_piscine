/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 14:58:08 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/10 17:49:04 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "headers.h"
#include <stdio.h>
int     *ft_map(int *tab, int length, int (*f)(int));

int square(int a)
{
	return a*a;
}

int* test(int *tab, int length, int (*f)(int))
{
	return ft_map(tab, length, f);
}

int main()
{
	int N = 11;
	int *tab;
	void (*f)(int);
	int (*f2)(int);
	int i;

	f = &ft_putnbr;
	tab = (int*)malloc(sizeof(int) * N);

	i = -1;
	while (++i < N)
		*(tab + i) = i;

	printf("Test 2. Square, Putnbr\n");

	f2 = &square;
	tab = test(tab, N, f2);
	i = -1;
	while (++i < N)
	{
		ft_putnbr(*(tab + i));
		ft_putchar(' ');
	}
	return 0;
}
