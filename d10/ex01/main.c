/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 14:58:08 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/10 17:32:13 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "headers.h"
#include <stdio.h>
void ft_foreach(int *tab, int length, void(*f)(int));

int square(int a)
{
	return a*a;
}

void test(int *tab, int length, void(*f)(int))
{
	ft_foreach(tab, length, f);
}

int main()
{
	int N = 5;
	int *tab;
	void (*f)(int);
	int (*f2)(int);
	int i;

	f = &ft_putnbr;
	tab = (int*)malloc(sizeof(int) * N);
	i = -1;
	while (++i < N)
	{
		*(tab + i) = i;
	}

	printf("test 1. Putnbr\n");
	test(tab, N, f);

	printf("\n\n");
	printf("Test 2. Square, Putnbr\n");

	f2 = &square;
	test(tab, N, f2);
	test(tab, N, f);

	return 0;
}
