#include "ft_strcmp.c"
#include <string.h>
#include <stdio.h>

void test(char *s1, char *s2) {
	int res = ft_strcmp(s1, s2);
	int orig = strcmp(s1, s2);
	printf("%s %s %d %d\n", s1, s2, res, orig);
}

int main() {
	test("hh", "haha");
	test("haha", "haha");
	test("haha", "hh");	

	return 0;
}
