#include "ft_strlen.c"
#include <string.h>
#include <stdio.h>

void test(char *s1) {
	int res = ft_strlen(s1);
	int orig = strlen(s1);
	printf("%s %d %d\n", s1, res, orig);
}

int main() {
	test("");
	test("s");
	test("hh");
	test("haha");
	test("hahaffjdskafldsa");	

	return 0;
}
